package com.hendisantika.springbootblogsample.service;

import com.hendisantika.springbootblogsample.entity.User;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 08:01
 */
public interface UserService {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    User save(User user);
}
