package com.hendisantika.springbootblogsample.service;

import com.hendisantika.springbootblogsample.entity.Post;
import com.hendisantika.springbootblogsample.entity.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 08:01
 */
public interface PostService {
    Optional<Post> findForId(Long id);

    Post save(Post post);

    void delete(Post post);

    /**
     * FInds a {@link Page} of {@link Post} of provided user ordered by date
     */
    Page<Post> findByUserOrderedByDatePageable(User user, int page);

    /**
     * Finds a @{link Page} of all {@link Post} ordered by date
     */
    Page<Post> findAllOrderedByDatePageable(int page);
}
