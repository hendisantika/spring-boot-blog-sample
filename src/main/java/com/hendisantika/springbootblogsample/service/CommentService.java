package com.hendisantika.springbootblogsample.service;

import com.hendisantika.springbootblogsample.entity.Comment;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 08:00
 */
public interface CommentService {
    Comment save(Comment comment);
}
