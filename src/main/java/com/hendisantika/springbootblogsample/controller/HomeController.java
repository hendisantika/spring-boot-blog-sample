package com.hendisantika.springbootblogsample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 08:02
 */
@Controller
public class HomeController {

    @GetMapping("/")
    public String home() {

        return "/home";

    }

}