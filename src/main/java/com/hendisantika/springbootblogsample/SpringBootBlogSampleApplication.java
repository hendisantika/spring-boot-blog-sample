package com.hendisantika.springbootblogsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBlogSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBlogSampleApplication.class, args);
    }

}
