package com.hendisantika.springbootblogsample.repository;

import com.hendisantika.springbootblogsample.entity.Comment;
import com.hendisantika.springbootblogsample.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 07:57
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {

    Optional<User> findByEmail(@Param("email") String email);

    Optional<User> findByUsername(@Param("username") String username);

}