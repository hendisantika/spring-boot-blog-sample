package com.hendisantika.springbootblogsample.repository;

import com.hendisantika.springbootblogsample.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 07:58
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(@Param("role") String role);

}