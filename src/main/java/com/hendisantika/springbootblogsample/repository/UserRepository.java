package com.hendisantika.springbootblogsample.repository;

import com.hendisantika.springbootblogsample.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-blog-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-13
 * Time: 07:59
 */
public interface UserRepository extends JpaRepository<User, Long> {
}